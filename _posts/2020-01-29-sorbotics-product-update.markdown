---
layout: post
comments: true
title:  "Actualización de los Productos de Sorbotics (Ene 2020)"
date:   2020-01-29 22:00:00 -0500
categories: [sorbotics, release]
---

## Introducci&oacute;n

Este es nuestro cuarto post anunciando cambios y mejoras a nuestros productos
y servicios. En Sorbotics valoramos la comunicaci&oacute;n con nuestros 
clientes. 

En este mes hemos completado una serie de funcionalidades que complementan las
que anunciamos el mes anterior. Otras nuevas funcionalidades también han sido
incluidas. Además se ha resuelto algunos error detectados y se ha mejorado los 
procesos de empaquetado e instalación de algunos productos.

## SDC

Se terminó el proceso de portar todas las aplicaciones que utiliza el SDC a 
Ubuntu 18.04.

## SDC-base

- Se añadió al packete .deb que requiera como dependencia de intalación Nodejs.
- Se terminó de migrar todo el proceso de creación y inserción de datos de la 
  instalación al uso de migraciones. Permitiendo que el processo de 
  actualización se realize solo utilizando los comandos 
  `apt-get update && apt-get upgrade`.

## Comm Service for Historic Data over

- Se añadió al packete .deb que requiera como dependencia de intalación el 
  paquete sdc-base.

## Sdc-housekeeper

- Se corrigió un error de segmentación que no permitía la ejecución de esta 
  aplicación en Ubuntu 18.04.
- Se añadió al packete .deb que requiera como dependencia de intalación el 
  paquete sdc-base.

## SDC Web Config Tool

### Actualizacion de la rama 2.x a la version 2.9 con el siguiente cambio.

- Se incluyó el manejo de la vista para los algoritmos de optimización.

### Actualizacion de la rama 3.x a la version 3.6.3-2 con las siguientes correciones.

- La visualización de los grupos cuando sus datos están sincronizados con la 
  plataforma que seguía mostrando que el grupo no estaba sincronizado.
- Se añadieron validaciones para algunos campos numéricos en el proceso de 
  importación desde un CSV que podrían tener valores incorrectos.

Además se continúan migrando los métodos que utilizan librerías antiguas para 
la conexión a las bases de datos MySQL por el uso de la librería Sequelize.

## SDC Tree Api

- Se añadió soporte para manejar en las urls de los nodos puertos no estándar.

## Almacenamiento en bases de datos de Review.

- Se añadió a la funcionalidad de escribir valores tipo 'string' a las bases de 
  datos de Review para Real Time y el Hot Data del historian. Anteriormente 
  estos valores eran ignorados.

## Machine Learning Trainer

- Se añadió un gráfico de sincronización en la pestaña 'Prediction' en la vista 
  de los Algoritmos de regresión.
- Se ajustaron las columnas de la tabla 'Work items' en la vista de los modelos.
- Se añadió un filtro en el componente de seleccion de Assets(process)
- Se igualaron las opciones del selector Source Type del formulario para Create 
  Dataset Version al las del mismo selector en Create Dataset Form.
- Se cambió el estilo de los botones en el panel 'Model' para que aunque el 
  modelos no este listo o falle estos se vean igual que si fuera Ok aunque 
  estén deshabilitados.
- Se corrigió un error en la sincronización de los Gráficos de Anomalias.
- Se actualizó el panel de las propiedades de los modelos acorde al uso de los 
  algoritmos de optimización.