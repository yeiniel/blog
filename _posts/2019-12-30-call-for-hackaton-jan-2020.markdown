---
layout: post
comments: true
title:  "Convocatoria a Hackaton Enero 2020"
date:   2019-12-30 13:49:00 -0500
categories: [sorbotics, hackaton-2020]
---

## Introducci&oacute;n

Convocamos a todos los que trabajamos en Sorbotics a participar en el
Hackaton que realizaremos en enero del 2020. En este post presentaremos
las caracter&iacute;sticas de este evento y las reglas para participar.

## Caracter&iacute;sticas

El objetivo del evento es para fomentar la motivaci&oacute;n de los 
participantes y la innovaci&oacute;n a lo interno de nuestro grupo.

El Hackaton se desarrollar&aacute; en el lapso de tiempo de 2 d&iacute;as laborables. Este lapso de tiempo contar&aacute; como tiempo trabajado para 
aquellos que trabajan con nosotros en la modalidad de por resultados.

La fecha del Hackaton esta por definirse. Tentativamente la fecha es 2-3 de 
enero (Jueves y Viernes). La idea es formar equipos de 2 miembros cada uno.

El producto a desarrollar por cada equipo debe ser aquel que sus miembros
consideren mas interesante o importante para Sorbotics. No debe existir
interferencia de los gestores de Sorbotics en cuando a la selecci&oacute;n del 
producto a desarrollar por parte de un equipo.

Al final del evento se seleccionar&aacute; el equipo ganador o ganadores.
La cantidad de equipos ganadores depende del destino final de cada producto
entregado. Si uno o mas productos se decide que seran aplicados o ofrecidos
como servicios en la empresa automaticamente estos equipos se nombran ganadores
y reciben un premio de acuerdo a una posicion (1er lugar, 2do lugar, etc). Caso
contrario se seleccionar&aacute; un &uacute;nico ganador. 

## Reglas

El resultado a evaluar de cada equipo debe ser un producto en s&iacute; 
(funcionalmente hablando). Aunque dado lo corto del evento se aceptaran 
entregas en forma de prototipos (codigo sin tests y que no podr&aacute; usarse 
en producci&oacute;n sin una fuerte refactorizaci&oacute;n/reescritura).

Todos los productos (sin importar su naturaleza, ya sea c&oacute;digo, 
dise&ntilde;o o documento) debe subirse como commit al servidor
gitlab.dev.sorbapp.com antes de las 12 de la madrugada del segundo d&iacute;a 
del evento. Cualquier producto que no se encuentre en el servidor o que no se
haya subido al mismo en el rango de tiempo que dura el hackaton sera 
descartado. Cualquier excepci&oacute;n a esta regla debe acordarse previamente.

Los repositorios que almacenan los productos parte del hackaton seran 
personales de cada usuario y accesibles a todos los que trabajamos en Sorbotics
aunque no ser&acute;n accesibles externamente.

El ganador (o los ganadores) ser&aacute;n definidos en base a los objetivos 
estrat&eacute;gicos de Sorbotics y los objetivos espec&iacute;ficos del 
Hackaton.