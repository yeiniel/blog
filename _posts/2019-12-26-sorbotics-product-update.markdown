---
layout: post
comments: true
title:  "Actualización de los Productos de Sorbotics (Dic 2019)"
date:   2019-12-26 11:25:00 -0500
categories: [sorbotics, release]
---

## Introducci&oacute;n

Este es nuestro segundo post anunciando cambios y mejoras a nuestros productos
y servicios. En Sorbotics valoramos la comunicaci&oacute;n con nuestros clientes 
as&iacute; que hemos decidido mantenernos realizando este tipo de 
comunicaciones. 

En este mes hemos completado una serie de funcionalidades que complementan las
que anunciamos el mes anterior. Otras nuevas funcionalidades tambien han sido
incluidas.

## Machine Learning Trainer

En este milestone hemos continuado mejorando nuestra oferta de Machine Learning 
Trainer. 

### Mejora a la Vista de Detalles de un Dataset

Ahora nuestros datasets se visualizan con informaci&oacute;n de tag ranking.
Esta pesta&ntilde;a solo est&aacute; disponible para datasets post-procesados
recientemente y que incluyan estos datos en sus resultados.

### Mejoras Generales a las Versiones de los Datasets

Ahora es posible incluir una descripci&oacute;n durante la creaci&oacute;n de 
nuevas versiones de un dataset. Una vez creada la versi&oacute;n del dataset se 
puede ver la descripci&oacute;n del mismo dando click en la vista resumen.

### Mejora a la Creaci&oacute;n de Modelos

Durante la creaci&oacute;n de modelos de tipo optimizaci&oacute;n ahora es 
posible usar n&uacute;meros en coma flotante para configurar varios de sus 
parametros en la vista de configuraci&oacute;n avanzada. Especificamente los 
siguientes par&aacute;metros:
 
 - Correlation threshold to discard feature
 - P-value threshold to discard feature
 - Feature importance threshold to discard feature
 - Acceptance rate
 - Optimization maximum epoch

### Mejora a la Lista de Modelos

Hemos enriquecido el dialogo que se abre en la lista de modelos para configurar
el despliegue de cada uno de ellos. Ahora este dialogo es tan potente
como la selecci&oacute;n de features en la diferentes gr&aacute;ficas de las 
vistas de detalles de datasets, modelos y offline predictos. No solo es posible 
seleccionar o de-seleccionar todos los SDC, tambien es posible ahora invertir
la selecci&oacute;n.

### Mejora a la Vista de Detalles de un Modelo

Ahora nuestros modelos de tipo clustering pueden incluir datos de tag ranking.
La pesta&ntilde;a que contiene esta informaci&oacute;n solo es visible si la 
informaci&oacute;n esta disponible para el modelo en cuesti&oacute;n (aquellos 
creados despues que el feature ha sido implementado en el entrenador).

Otro cambio asociado a la pesta&ntilde;a de tag ranking es que ahora esta vista
opcionalmente incluye un selector cuando existan varias fuentes de datos. De
esta forma se pueden visualizar diferentes tag rankings para un mismo modelo.

Otro cambio a la vista de detalles es la inclusi&oacute;n de soporte para 
modelos de tipo Digital Twin. Este es un nuevo tipo de modelo que estamos 
incluyendo en nuestra oferta de machine learning.

### Mejoras Generales a las Predicciones Offline

Ahora es posible incluir una descripci&oacute;n durante la creaci&oacute;n de 
nuevas Predicciones Offline. Una vez creada se puede ver la descripci&oacute;n 
de la misma dando click en la vista de detalles.

### Mejora a la Vista de Detalles de Predicciones Offline

La vista de detalles de Predicciones Offline para quellas creadas a partir de 
modelos de tipo optimizaci&oacute;n ha sido refactorizada para incluir la 
informaci&oacute;n que es verdaderamente relevante para este tipo de modelo. 

Hemos incluido una pesta&ntilde;a a la vista de detalles de las predicciones
offline para Predicciones Offline creadas a partir de modelos de tipo 
optimizaci&oacute;n. En ella mostramos los detalles
del modelo a partir del cual se realiza la predicci&oacute;n. 

Otro cambio en esta vista es la adicion de otra pesta&ntilde; para el caso de
offline predictors creados a partir de modelos de tipo optimizaci&oacute;n. En 
ella mostramos los datos CUMSUM. Esta pesta&ntilde;a solo est&acute; disponible
para offline predictors creados recientemente y que incluyan estos datos en sus
resultados.

Ahora nuestros offline predictors pueden incluir datos de tag ranking.
La pesta&ntilde;a que contiene esta informacion solo es visible si la 
informacion esta disponible para el offline predictor en cuesti&oacute;n 
(aquellos creados despues que el feature ha sido implementado en el entrenador).

Otro cambio a la vista de detalles es la inclusi&oacute;n de soporte para 
aquellos offline predictos creados a partir de modelos de tipo Digital Twin.
Este es un nuevo tipo de modelo que estamos incluyendo en nuestra oferta de 
machine learning. 

## Sorbotics Data Collector

Para despliegues existentes de las versiones 1.x, 3.x, 4.x y 5.x del SDC la
actualizaci&oacute;n debe realizarse de forma manual.

Existia un problema en los SDC legacy (v1.x) en los cuales modelos de machine 
learning que habian sido removidos se quedaban ocupando espacio en el medio de 
almacenamiento del dispositivo. Se ha modificado el software encargado de 
eliminar esos modelos del medio de almacenamiento para que sea mas efectivo
al realizar la eliminacion y que informe en sus logs en caso de existir una
condicion que impida su correcto funcionamiento.

### Problemas Corregidos al Web Config Tool

Existia un problema en el proceso de importar/exportar en los SDC
que hacia que se perdiera informacion necesaria de los modelos. Especificamente
el mapeo al atributo referenceTag. Este problema ha sido corregido.

De forma general se ha perfeccionado la forma en que empaquetamos nuestro 
producto para produccion. Por lo que nuestros clientes deben notar una mejora
significativa en la velocidad de carga del WCT en sus navegadores.

## Sorbotics Platform

Hemos incluido soporte para despliegue de modelos de tipo Digital Twin en 
nuestra plataforma. Este es un nuevo tipo de modelo que estamos incluyendo en
nuestra oferta de machine learning. Con este cambio equiparamos las capacidades
de despliegue de la plataforma a las existentes en nuestra oferta de machine
learning para SDC que usan el mecanismo legacy al efecto.

Hemos incluido soporte para el driver DMX Banner MTCP. Este nuevo driver
fue incluido en el release anterior del SDC y con este cambio completamos
la paridad de features entre el WCT y la plataforma.

Otro cambio a la plataforma es la inclusi&oacute;n de la capacidad de manejo 
para el caso de nodos que no puedan parsearse como JSON v&aacute;lido. Este
es un cambio bien importante que permite librarse de nodos corruptos.  

## Conclusiones

Seguimos trabajando en mejorar nuestra suite de productos y servicios. 
Trataremos de mantenerlos informados peri&oacute;dicamente al respecto. El 2020
viene con mejoras y nuevos features. Mantengase en contacto.