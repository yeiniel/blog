---
layout: post
comments: true
title:  "Actualización de los Productos de Sorbotics (Nov 2019)"
date:   2019-11-27 16:44:00 -0500
categories: [sorbotics, release]
---

## Introducci&oacute;n

Este es nuestro primer post anunciando cambios y mejoras a nuestros productos
y servicios. En Sorbotics valoramos la comunicaci&oacute;n con nuestros clientes 
as&iacute; que hemos decidido realizar este post. 

En Sorbotics tratamos de ser &aacute;giles en el proceso de desarrollo de nuestros 
productos. Este mes nos hemos concentrado en nuestra oferta de Machine Learning
Trainer. En este post vamos a presentar las nuevas funcionalidades que
hemos hecho disponible en nuestra oferta de servicio.

## Machine Learning Trainer

### Cambios a los An&aacute;lisis

#### Modelos de Algoritmo tipo Digital Twin

Hemos incorporado un nuevo tipo de algoritmo para la creaci&oacute;n de modelos a 
nuestro repertorio: Digital Twin.

#### Capacidad de Online Learning para todos los Algoritmos

Hemos incorporado la posibilidad de realizar Online Learning para todos los
tipos de algoritmos empleados en la creaci&oacute;n de modelos. Durante la 
creaci&oacute;n de nuevos an&aacute;lisis en el paso de selecci&oacute;n de Algoritmo al
dar click en el bot&oacute;n "Advance Settings" en esa vista ahora se muestra
un checkbox para seleccionar esta opci&oacute;n. Adicionalmente en la vista de
detalles de cada modelo en el lado izquierdo se incluye una nueva tabla
para mostrar si se seleccion&oacute; esa opci&oacute;n durante la creaci&oacute;n del 
an&aacute;lisis.

#### Posibilidad de Eliminar una Regla de Proyecto

Ahora es posible eliminar una regla de proyecto. Durante la creaci&oacute;n de un 
nuevo An&aacute;lisis en el paso de selecci&oacute;n de Features al dar click en el bot&oacute;n
"Add and Select rules" las reglas existentes pueden ser seleccionadas para su
uso y eliminadas.

#### Descripci&oacute;n de un An&aacute;lisis

Ahora es posible incluir una descripci&oacute;n durante la creaci&oacute;n de nuevos an&aacute;lisis.
Una vez creado el modelo se puede ver la descripci&oacute;n del an&aacute;lisis dando click en
un proyecto en la lista de proyectos y a continuaci&oacute;n en el bot&oacute;n "Properties"
del an&aacute;lisis en cuesti&oacute;n.

### Vista de Tag Ranking para modelos de tipo Optimizaci&oacute;n

Ahora nuestros modelos de tipo optimizaci&oacute;n se visualizan con un gr&aacute;fico de tag ranking.

### Mejora a la Lista de Predicciones Offline

Ahora la lista de Predicciones Offline incluye junto a cada una el Dataset
y la Versi&oacute;n del mismo a partir de la cual se realiz&oacute; la predicci&oacute;n.

### Mejora a la Vista de Detalles de un Dataset

La vista de detalles de un dataset presentaba un bug. En esta vista se visualizan
varias fuentes de datos y el orden en que las solicitudes de estos datos se
completaban pod&iacute;an producir que el Tab de Sumario se quedara en
blanco. Este problema ha sido resuelto.

## Sorbotics Data Collector

### Nuevos Drivers

El paquete de software Sorbotics Data Collector ha recibido dos nuevos drivers

 - DXM Banner MTCP
 - OPC-UA Mejorado

Actualmente el nuevo driver OPC-UA se considera estable y est&aacute; disponible de
forma general para nuevos despliegues del paquete de softwares en el sistema
operativo Ubuntu version 18.04. 

Para despliegues existentes de las versiones 1.x, 3.x, 4.x y 5.x del SDC la
actualizaci&oacute;n debe realizarse de forma manual.

El driver DXM Banner MTCP se considera en desarrollo por el momento pero 
esperamos hacerlo disponible en general en corto tiempo.

### Problemas Corregidos al Web Config Tool

Exist&iacute;a un bug que imped&iacute;a la carga correcta de todas las opciones en el
formulario de configuraci&oacute;n de un Tag cuando se cambiaba el tipo de canal
asociado. El bug se hac&iacute;a evidente cuando se seleccionaba un tipo de canal
que requer&iacute;a informaci&oacute;n del campo para la configuraci&oacute;n del tag. Este bug
ha sido corregido.

Las acciones de Reiniciar el SDC y apagarlo desde el Web Config Tool 
fallaban por un bug en la codificaci&oacute;n de las operaciones. Este problema estaba
presente en las versiones posteriores a 3.0. Este bug ha sido
corregido.

### Otras Mejoras

El proceso de empaquetado del software del SDC se ha mejorado de forma que
la actualizaci&oacute;n del software incluye la actualizaci&oacute;n del esquema de las 
diferentes bases de datos que le dan soporte a su funcionamiento. A partir
de este punto el proceso de actualizaci&oacute;n de software puede hacerse
completamente automatizado y de forma muy simple.

## Conclusiones

Seguimos trabajando en mejorar nuestra suite de productos y servicios. Trataremos
de mantenerlos informados peri&oacute;dicamente al respecto.